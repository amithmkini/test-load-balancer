# Test Load Balancer

## Instructions

### Build all 3 docker images:
- ```docker build -t test-load-balancer .```
- ```cd Server1```
- ```docker build -t server1 .```
- ```cd ../Server2```
- ```docker build -t server2 .```

### Docker run
- ```docker run --name s1 -d -p 8001:80 server1```
- ```docker run --name s2 -d -p 8002:80 server2```
- ```docker run --name balance -d -p 80:80 test-load-balancer```
